%bcond_without rhcd_compat
%bcond_without check

# https://github.com/redhatinsights/rhc
%global goipath         github.com/redhatinsights/rhc
Version:                0.3.1
%global tag             v%{version}

%gometa -f

%global golicenses      LICENSE
%global godocs          CONTRIBUTING.md README.md

%global setup_flags     -Dvendor=True
%if %{with rhcd_compat}
%global setup_flags     %{setup_flags} -Drhcd_compatibility=True
%endif

Name:    rhc
Release: %autorelease
Epoch:   1
Summary: rhc connects the system to Red Hat hosted services
License: GPL-3.0-only
URL:     %{gourl}

Source: %{url}/releases/download/%{tag}/%{name}-%{version}.tar.xz

Recommends:         insights-client

Requires:           subscription-manager
Requires:           yggdrasil >= 0.4
Requires:           yggdrasil-worker-package-manager
Requires(post):     policycoreutils-python-utils

BuildRequires:      git-core
BuildRequires:      golang
BuildRequires:      go-rpm-macros
BuildRequires:      meson
BuildRequires:      systemd-rpm-macros
BuildRequires:      pkgconfig(dbus-1)
BuildRequires:      pkgconfig(systemd)
BuildRequires:      pkgconfig(bash-completion)

%global common_description %{expand:
%{name} is a client tool and daemon that connects the system to Red Hat hosted
services enabling system and subscription management.}

%description
%{common_description}

%prep
%goprep %{?rhel:-k}
%autopatch -p1

%if %{undefined rhel}
%generate_buildrequires
%go_generate_buildrequires
%endif

%build
%undefine _auto_set_build_flags
export %gomodulesmode
%{?gobuilddir:export GOPATH="%{gobuilddir}:${GOPATH:+${GOPATH}:}%{?gopath}"}
%meson %{setup_flags} "-Dgobuildflags=[%(echo %{expand:%gocompilerflags} | sed -e s/"^"/"'"/ -e s/" "/"', '"/g -e s/"$"/"'"/), '-tags', '"rpm_crashtraceback\ ${BUILDTAGS:-}"', '-a', '-v', '-x']" -Dgoldflags='%{?currentgoldflags} -B 0x%(head -c20 /dev/urandom|od -An -tx1|tr -d " \n") -compressdwarf=false -linkmode=external -extldflags "%{build_ldflags} %{?__golang_extldflags}"'
%meson_build

%install
%meson_install
install --directory %{buildroot}%{_unitdir}
%if %{with rhcd_compat}
install --directory %{buildroot}%{_sysconfdir}/rhc
%endif

%if %{with check}
%check
%gocheck
%endif

%pre
%if %{with rhcd_compat}
if [ $1 -eq 2 ]; then
    if [ -f /etc/rhc/config.toml ]; then
        cp /etc/rhc/config.toml /etc/yggdrasil/config.toml.migrated
    fi
fi
%endif

%post
/usr/sbin/semanage permissive --add rhcd_t || true
%systemd_post rhc-canonical-facts.timer
if [ $1 -eq 1 ]; then
     systemctl daemon-reload
     systemctl start rhc-canonical-facts.timer
fi
%if %{with rhcd_compat}
if [ $1 -eq 2 ]; then
    if [ -f /etc/yggdrasil/config.toml.migrated ]; then
        sed -E 's#^broker( ?=)#server\1#' /etc/yggdrasil/config.toml.migrated > /etc/yggdrasil/config.toml
        echo 'facts-file = "/var/lib/yggdrasil/canonical-facts.json"' >> /etc/yggdrasil/config.toml
        rm /etc/yggdrasil/config.toml.migrated
    fi
fi
%endif

%preun
%systemd_preun rhc-canonical-facts.timer

%postun
if [ $1 -eq 0 ]; then
    /usr/sbin/semanage permissive --delete rhcd_t || true
fi
%systemd_postun_with_restart rhc-canonical-facts.timer
if [ $1 -eq 0 ]; then
     systemctl daemon-reload
fi

%files
%license LICENSE
%doc CONTRIBUTING.md README.md
%{_bindir}/*
%{_datadir}/bash-completion/completions/*
%{_mandir}/man1/*
%{_unitdir}/rhc-canonical-facts.*
%if %{with rhcd_compat}
%{_unitdir}/yggdrasil.service.d/rhcd.conf
%endif

%changelog
%autochangelog
